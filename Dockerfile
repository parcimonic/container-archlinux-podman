# hadolint ignore=DL3007
FROM archlinux:latest

RUN set -euf && \
    pacman -Syu --noconfirm --noprogressbar \
    ansible \
    ansible-lint \
    fuse-overlayfs \
    molecule \
    molecule-plugins \
    podman

RUN sed -i "s,^#netns.*,netns=\"host\"," /etc/containers/containers.conf
