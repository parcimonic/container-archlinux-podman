# Arch Linux container with Podman

This is an [Arch](https://archlinux.org/) based image that I'm using for my
GitLab automation pipelines on selfhosted runners.

It has Podman preinstalled so I can skip it's install steps on my Molecule jobs.

URL: `registry.gitlab.com/parcimonic/container-archlinux-podman:latest`

## References

I've used these sources to figure out how to make it work:

- https://www.redhat.com/sysadmin/podman-inside-container

- https://github.com/containers/podman/issues/11969
